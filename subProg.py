import os
import json
import datetime
import pandas as pd
import numpy as np
import cv2
from time import sleep
from numpy import unique
from scipy.signal import medfilt
# from tensorflow_core.python.keras.preprocessing.image import img_to_array
from tensorflow.keras.models import load_model
import xlsxwriter
import requests

tag_header = 'E200001909'
tag_header_size = len(tag_header)
model = load_model('fine_vgg19.h5')
file_types = (".json", ".mp4")
folder_sep = os.path.sep

under_samp = 2
window_size = 3
marker_window_size = 21 #31
acc_tresh = 0.35 #0.7
batch_size = 1000 #frames evaluados simultaneamente en un batch
# mean = np.array([123.68, 116.779, 103.939][::-1], dtype="float32") #rgb
input_size = (224, 224)
mean = np.array([103.939, 116.779, 123.68][::-1], dtype="float32")  # bgr
data_dict = {
    1: 'V',
    2: 'A'
}
url = "https://siteplanillas.azurewebsites.net/api/Consulta"
headers = {'Content-type': 'application/json', 'Accept': '*/*'}

def list_files(basePath, contains=None):
    return list_files_in_route(basePath, validExts=file_types, contains=contains)


def check_new_json (jsons,vids_folder):
    new_jsons = []
    for json in jsons:
        json_name = json.split(folder_sep)[-1]
        vid_name = json_name.split("-")[0]
        flag = json_name[0:4]
        if (not flag == "PROC") and (os.path.exists(os.path.join(vids_folder, vid_name + ".mp4"))):
            new_jsons.append(json)
    return new_jsons


def get_data_from_json(json_path):
    json_name = json_path.split(folder_sep)[-1]
    data_in_name = json_name.split('-')
    sector = data_in_name[1].split(".")
    vid_name = data_in_name[0]

    with open(json_path) as json_file:
        rfid_data = json.load(json_file)
    #sorted_data = [bytes.fromhex(srtdData['tagData']).decode('ASCII') for srtdData in sorted(data, key=str2dateTime)]
    #sorted_data = [bytes.fromhex(srtdData['tagData']).decode('ASCII') for srtdData in sorted(rfid_data, key=str2dateTime) if (srtdData['tagData'][0:tag_header_size] == tag_header)]
    sorted_data = [srtdData['tagData'] for srtdData in sorted(rfid_data, key=str2dateTime) if (srtdData['tagData'][0:tag_header_size]==tag_header)]

    return (vid_name, sector[0], vid_name.split('_')[1], sorted_data)


def flag_json(json):
    split_route = json.split(folder_sep)
    json_name = "PROC_" + split_route[-1]
    split_route[-1] = json_name
    os.rename(json, os.path.join(*split_route))


def get_autohome_data(xcel_route):
    secuencia_almacen = []
    excel = pd.read_excel(xcel_route).values
    resumen_almacen = []
    for cell in excel:
        datos_isla = {
            'sector': cell[0],
            'isla': cell[1],
            'filas': cell[2],
            'columnas': cell[3],
            'tipo_inicio': cell[4]
        }
        resumen_almacen.append(datos_isla)

    sectores = sorted(unique([sect['sector'] for sect in resumen_almacen]))
    for sector in sectores:
        resumen_sector = [isla_sect for isla_sect in resumen_almacen if isla_sect['sector'] == sector]
        secuencia_filas = [elm['filas'] for elm in resumen_sector]
        secuencia_columnas = [elm['columnas'] for elm in resumen_sector]
        secuencia_sector = {
            'sector': sector,
            'secuencia': obtener_secuencia_islas(resumen_sector),
            'filas': secuencia_filas,
            'columnas': secuencia_columnas
        }
        secuencia_almacen.append(secuencia_sector)

    return secuencia_almacen


def process_video(video_route):
    # Esperar a que el video termine de copiarse
    file_size = 0
    while True:
        file_info = os.stat(video_route)
        if file_info.st_size == 0 or file_info.st_size > file_size:
            file_size = file_info.st_size
            sleep(0.5)
        else:
            break
    # Si el video está completo, procesar
    vs = cv2.VideoCapture(video_route)
    (W, H) = (None, None)
    batch_index = 0
    batch_images = []
    frame_index = 0

    data = []
    marker = []

    while True:
        (grabbed, frame) = vs.read()
        if not grabbed:
            if batch_index != 0:
                (data_filt, marker_filt) = predict_batch(batch_images)
                data += data_filt
                marker += marker_filt

            batch_images.clear()
            break
        else:
            frame_index += 1
        if W is None or H is None:
            (H, W) = frame.shape[:2]

        if frame_index % under_samp == 0:
            if H > W:
                frame = cv2.rotate(frame, cv2.ROTATE_90_CLOCKWISE)
            image = cv2.resize(frame, input_size)
            # image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)#
            # image = img_to_array(image)
            image = image.astype("float32")  #
            image -= mean
            image = np.expand_dims(image, axis=0)
            batch_images.append(image)
            batch_index += 1

        if batch_index == batch_size:
            (data_filt, marker_filt) = predict_batch(batch_images)
            data += data_filt
            marker += marker_filt
            batch_images.clear()
            batch_index = 0
    vs.release()
    register = get_register(np.array(data), np.array(marker))
    return register


def generate_result(rfid_data, autohome_data, video_data, sector, results_folder, flight_date, file_name):
    workbook = xlsxwriter.Workbook(os.path.join(results_folder, file_name+'.xlsx'))
    worksheet = workbook.add_worksheet()
    row = 0
    col = 0
    titles = ['VIN', 'Almacén', 'Isla', 'Fila', 'Columna']
    for title in titles:
        worksheet.write(row, col, title)
        col += 1
    row += 1
    col = 0

    cells_seq = [item['secuencia'] for item in autohome_data if str(item['sector']) == sector][0]
    rows_seq = [item['filas'] for item in autohome_data if str(item['sector']) == sector][0]
    cols_seq = [item['columnas'] for item in autohome_data if str(item['sector']) == sector][0]

    json_content = []
    #print(len(rfid_data))
    #print(len(video_data))
    #print(len(cells_seq))

    k = 0  #indice de la secuencia de celdas
    j = 0  #contador de data rfid
    l = 0
    for m, col_size in enumerate(rows_seq):
        cols_in_isle = cols_seq[m]
        for col_index in range(l, l+cols_in_isle):
            detected_in_column = video_data[col_index]
            #print(detected_in_column)
            #print(len(detected_in_column))
            error = len(detected_in_column) - col_size
            if error > 0:
                detected_in_column = detected_in_column[:-error]
            elif error < 0:
                detected_in_column = detected_in_column.ljust(col_size, 'V')

            #print(detected_in_column)
            #print(len(detected_in_column))
            for i, cell_name in enumerate(cells_seq[k:k+col_size]):
                vin = "-"

                if (detected_in_column[i] == 'A' and j < len(rfid_data)):
                    vin = rfid_data[j]
                elif (detected_in_column[i] == 'A' and j >= len(rfid_data)):
                    vin = "vin?"
                """
                cell = {
                    'Cell': cell_name,
                    'VIN': vin,
                    'FechaInv': flight_date,
                    'FechaProc': file_name
                }
                """

                cell_name = cell_name.split('-')
                cell = {
                    'vin': vin,
                    'almacen': 'VARI 90',
                    'isla': cell_name[0],
                    'fila': cell_name[1][1:],
                    'columna': cell_name[1][0]
                }
                #post to VariSystem api
                #cell_name = cell_name.split('-')
                #data = {'vin': vin, 'almacen': 'VARI 90', 'isla': cell_name[0], 'fila': cell_name[1][1:], 'columna': cell_name[1][0],'envioCorreo': False}
                #requests.get(url, json=data, headers=headers)
                #r=requests.get(url, json=data, headers=headers)
                #print(r)
                json_content.append(cell)

                if detected_in_column[i] == 'A':
                    j += 1
            k += col_size
        l += cols_in_isle + 1

    # POST to VariSystem API
    #requests.get(url, json=json.dumps(json_content), headers=headers)
    data = json.dumps(json_content)
    print(data)
    r = requests.get(url, data=data, headers=headers)
    print("ENVIANDO DATA A VARI SYSTEM")
    print(r)

    with open(os.path.join(results_folder, file_name+'.json'), 'w') as fout:
        json.dump(json_content, fout)

    for cell_info in json_content:
        worksheet.write(row, col, cell_info.get('vin'))
        col += 1
        worksheet.write(row, col, cell_info.get('almacen'))
        col += 1
        worksheet.write(row, col, cell_info.get('isla'))
        col += 1
        worksheet.write(row, col, cell_info.get('fila'))
        col += 1
        worksheet.write(row, col, cell_info.get('columna'))
        col = 0
        row += 1
    workbook.close()

# --------> FUNCIONES QUE NO SE LLAMAN EN EL PRINCIPAL


def predict_batch(images):
    # ordenar y clasificar imágenes del batch
    images = np.vstack(images)
    preds = model.predict(images)

    data = []
    marker = []
    prev = 2.5
    for pred in preds:
        i = np.argmax(pred)
        acc = pred[i]
        if acc < acc_tresh:
            if i > 2 and prev < 2:
                data.append(prev)
                marker.append(-1)
            elif i >= 2 and prev == 2:
                data.append(-1)
                marker.append(prev)
            else:
                data.append(-1)
                marker.append(-1)
                prev = 2.5
        elif i > 2:
            data.append(-1)
            marker.append(-1)
            prev = 2.5
        elif i == 2:
            marker.append(i)
            data.append(-1)
            prev = 2
        else:
            data.append(i)
            marker.append(-1)
            prev = i

    # Filtro mediana para eliminar ruido
    data_filt = medfilt(data, window_size)
    marker_filt = medfilt(marker, marker_window_size)

    data = []
    marker = []
    for d, m in zip(data_filt, marker_filt):
        if m > -1:
            data.append(-1)
            marker.append(2)
        else:
            data.append(d)
            marker.append(-1)

    return data, marker


def get_register(data, marker):
    d0 = -1
    m0 = -1
    register = []
    for now in range(data.size):
        d1 = data[now]
        m1 = marker[now]
        if d0-d1 > 0:
            register.append(data_dict[d0-d1])
        if m1 != m0:
            register.append('M')
        d0 = d1
        m0 = m1
    init = register[0]
    register = ''.join([str(elem) for elem in register])
    register = [elem for elem in register.split('M') if len(elem) > 10]
    #register = list(filter(len, register.split('M')))
    if init != 'M':
        register = register[0:]
    register = [elem for elem in register if len(elem) > 25]
    return register


def obtener_secuencia_islas(resumen_sector):
    secuencia_sector = []
    for isla in resumen_sector:
        celdas = []
        col_names = [chr(c) for c in range(65, 65 + isla['columnas'])]
        tipo_inicio = isla['tipo_inicio']
        filas = isla['filas']
        isla = isla['isla']

        if (tipo_inicio >= 2):
            col_names.reverse()
        num = 1
        down = True
        if (tipo_inicio == 1) or (tipo_inicio == 2):
            num = filas
            down = False

        for col in col_names:
            for fil in range(filas):
                if down:
                    celdas.append(str(isla) + '-' + col + str(num + fil))
                else:
                    celdas.append(str(isla) + '-' + col + str(num - fil))
            num = filas if down else 1
            down = not down
        secuencia_sector += celdas
    return secuencia_sector


def str2dateTime(item):
    return datetime.datetime.strptime(item['dateTime'], '%H:%M:%S:%f')


def list_files_in_route(basePath, validExts=None, contains=None):
    for (rootDir, dirNames, filenames) in os.walk(basePath):
        for filename in filenames:
            if contains is not None and filename.find(contains) == -1:
                continue
            ext = filename[filename.rfind("."):].lower()
            if validExts is None or ext.endswith(validExts):
                imagePath = os.path.join(rootDir, filename)
                yield imagePath
