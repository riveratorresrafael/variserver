import os
from subProg import list_files, check_new_json, get_data_from_json, flag_json, get_autohome_data, \
    process_video, generate_result

# --> NO USAR EL SEPARADOR '/' EN LAS RUTAS. EN CAMBIO USAR '\\'. NO COLOCAR EL SEPARADOR AL FINAL DEL FOLDER
jsons_folder = 'jsons'
vids_folder = "vids"


def process_data(file_name, results_folder):
    autohome_data = get_autohome_data('plano_vari90.xlsx')
    #print(autohome_data)
    jsons = list(list_files(jsons_folder))
    #print(len(jsons))
    new_jsons = check_new_json(jsons, vids_folder)
    #print(len(new_jsons))
    if len(new_jsons) > 0:
        for json in new_jsons:
            (vid_name, sector, flight_date, rfid_data) = get_data_from_json(json)
            #print(vid_name)  #
            #print(sector)
            #print(date)
            #print(rfid_data)  #
            flag_json(json)
            video_data = process_video(os.path.join(vids_folder, vid_name+".mp4"))
            #video_data = ['AAVVVVVVVVVVVAVVVAVVVAVVVVVAAVVAVVVVAAVVAVAVVVVV', 'VVVVVAAAVVAVVAAAVAAAAAAVVAVAVAVVAAAVVVVVVVAAAAA', 'AAAAVAVVVVVAAVAAAAAAVVVVAVVAAAVVVVVVVVAAAVVVVV', 'VVVVVAVAVVVVAVVVAVVVVVVVAVVVAVAAVVVAAVVVVVVVAAAAA']
            print(video_data)
            #print({i: video_data.count(i) for i in video_data})
            #generate_result(rfid_data, autohome_data, video_data, sector, results_folder, flight_date, file_name)
            generate_result(rfid_data, autohome_data, video_data, sector, results_folder, flight_date, vid_name)
