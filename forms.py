from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField
from flask_wtf.file import FileField, FileAllowed
from wtforms.validators import Optional


class MainForm(FlaskForm):
    jsons = FileField('Jsons:', validators=[Optional(), FileAllowed(['json'], 'Solo archivos json!')])
    almacen = StringField('Almacén:', validators=[Optional()])
    generate = SubmitField('Procesar data')
    download = SubmitField('Descargar resultados')