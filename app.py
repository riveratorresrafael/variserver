from flask import Flask
from flask import render_template, url_for, request, redirect, send_from_directory, flash
from forms import MainForm
from backend import process_data
from datetime import datetime
from os import path
import webbrowser

app = Flask(__name__)
app.config['SECRET_KEY'] = 'asdawkl2k34l23kn4kbk234g3v42h3b'
file_name = datetime.now().strftime("%d_%m_%Y")
output_path = 'static\\output'


@app.route('/',  methods=['GET', 'POST'])
def main():
    form = MainForm()

    if form.validate_on_submit():
        if form.generate.data:
            jsons = request.files.getlist("jsons")
            for json in jsons:
                json.save(path.join('jsons', json.filename))
            process_data(file_name, output_path)
            flash('enabled', 'enable-down')
            return redirect(url_for('main'))
        elif form.download.data:
            print(file_name)
            if path.exists(path.join(output_path, file_name+'.json')):
                return send_from_directory(directory='static', filename='output/'+file_name + '.json', as_attachment=True)
            else:
                return send_from_directory(directory='static', filename='output/'+'FAILED.rar', as_attachment=True)
    return render_template('main2.html', form=form)


if __name__ == '__main__':
    webbrowser.open("http://127.0.0.1:5000/")
    app.run()
